# resource "aws_iam_user" "terraform" {
#   name          = "terraform"
#   path          = "/"
#   force_destroy = true
# }

# resource "aws_iam_user_login_profile" "terraform" {
#   user    = aws_iam_user.terraform.name
#   password_reset_required = true
#   pgp_key = "keybase:amoltete"
# }



resource "aws_iam_role" "ec2-role" {
  name = "ec2-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ec2-role-policy" {
  name = "ec2-role-policy"
  role = "${aws_iam_role.ec2-role.id}"
  policy = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": [
               "arn:aws:s3:::phuntru",
               "arn:aws:s3:::phuntru/*"
            ]
        },
        {
          "Effect": "Allow",
          "Action": "s3:ListAllMyBuckets",
          "Resource: "arn:aws:s3:::*"
        }
    ]
}
  EOF
}

resource "aws_iam_instance_profile" "ec2-role" {
  name = "ec2-role"
  role = ["${aws_iam_role.ec2-role.name}"]
}

resource "aws_instance" "role-instance" {
  ami = var.ami_id
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.sg1.id]
  key_name = "amol"
  iam_instance_profile = "${aws_iam_instance_profilr.ec2-role.name}"
   tags = {
      "Name": "role-instance"
   }
}