variable "ami_id" {
  type = string
  default = "ami-0fa49cc9dc8d62c84"
  description = "AMI-ID"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "vpc_id" {
    default = "vpc-01661835f46a6af8c" 
}

variable "key_name" {
  default = "amol"
}
