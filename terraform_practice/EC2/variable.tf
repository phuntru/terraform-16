variable "ami_id" {
    default = "ami-0fa49cc9dc8d62c84"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "amol"
}

variable "private_subnet_id" {
    default = "subnet-073391f7ef6fdd4c9"
}

variable "public_subnet_id" {
    default = "subnet-073391f7ef6fdd4c9"
}

variable "sg_id" {
    type = list(string) 
    default = [ "sg-0367d4863baff73d9" ]
}